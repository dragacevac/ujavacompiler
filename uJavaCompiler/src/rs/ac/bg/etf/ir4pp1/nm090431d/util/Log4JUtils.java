package rs.ac.bg.etf.ir4pp1.nm090431d.util;

import java.net.URL;

public class Log4JUtils {

	private static Log4JUtils logs = new Log4JUtils();

	public static Log4JUtils instance() {
		return logs;
	}

	public URL findLoggerConfigFile() {
		return Thread.currentThread().getContextClassLoader()
				.getResource("log4j.xml");
	}
}
