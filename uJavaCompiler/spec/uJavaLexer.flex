//import section

package rs.ac.bg.etf.ir4pp1.nm090431d;
import java_cup.runtime.Symbol;

%%

//JFLEX Directives section

%{

	private StringBuffer stringBuffer = new StringBuffer();
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol newSymbol(int type) {
		return new Symbol(type, yyline + 1, yycolumn);
	}
	
	// ukljucivanje informacije o poziciji tokena
	private Symbol newSymbol(int type, Object value) {
		return new Symbol(type, yyline+1, yycolumn, value);
	}
	private void lexicError()
	{
		System.err.println("Leksicka greska ("+yytext()+") u liniji "+ (yyline+1) + " u koloni "+ (yycolumn+1));
	}

%}


//CUP spec
//CUP line cnt
//CUP column cnt

%cup
%line
%column

%xstate COMMENT
%xstate STRING
%xstate CHAR

%eofval{ 

return newSymbol(Sym.EOF);

%eofval}

%%

//Regular Expressions sections

" " 	{ }
"\b" 	{ }
"\t" 	{ }
"\r\n" 	{ }
"\f" 	{ }


"program"   { return newSymbol(Sym.PROG, yytext()); }
"print" 	{ return newSymbol(Sym.PRINT, yytext()); }
"return" 	{ return newSymbol(Sym.RETURN, yytext()); }
"void" 		{ return newSymbol(Sym.VOID, yytext()); }
"break"		{ return newSymbol(Sym.BREAK, yytext()); }
"class"		{ return newSymbol(Sym.CLASS, yytext()); }
"if"		{ return newSymbol(Sym.IF, yytext()); }
"else"		{ return newSymbol(Sym.ELSE, yytext()); }
"while"		{ return newSymbol(Sym.WHILE, yytext()); }
"const"		{ return newSymbol(Sym.CONST, yytext()); }
"new"		{ return newSymbol(Sym.NEW, yytext()); }
"read"		{ return newSymbol(Sym.READ, yytext()); }
"extends"	{ return newSymbol(Sym.EXTENDS, yytext()); }

"+" 		{ return newSymbol(Sym.PLUS, yytext()); }
"-"			{ return newSymbol(Sym.MINUS, yytext()); }
"="			{ return newSymbol(Sym.EQUAL, yytext()); }
"."			{ return newSymbol(Sym.POINT, yytext()); }
"," 		{ return newSymbol(Sym.COMMA, yytext()); }
";" 		{ return newSymbol(Sym.SEMICOLON, yytext()); }
"(" 		{ return newSymbol(Sym.L_PARENT, yytext()); }
")" 		{ return newSymbol(Sym.R_PARENT, yytext()); }
"{" 		{ return newSymbol(Sym.L_CURLY, yytext()); }
"}"			{ return newSymbol(Sym.R_CURLY, yytext()); }
"["			{ return newSymbol(Sym.L_BRACKET, yytext()); }
"]"			{ return newSymbol(Sym.R_BRACKET, yytext()); }
"*"			{ return newSymbol(Sym.MUL, yytext()); }
"/"			{ return newSymbol(Sym.DIV, yytext()); }
"%"			{ return newSymbol(Sym.MOD, yytext()); }
"=="		{ return newSymbol(Sym.EQUAL_EQUAL, yytext()); }
"!="		{ return newSymbol(Sym.NOT_EQUAL, yytext()); }
"<"			{ return newSymbol(Sym.LESS, yytext()); }
">"			{ return newSymbol(Sym.GREATER, yytext()); }
"<="		{ return newSymbol(Sym.LESS_OR_EQUAL, yytext()); }
">="		{ return newSymbol(Sym.GREATER_OR_EQUAL, yytext()); }
"&&"		{ return newSymbol(Sym.LOGIC_AND, yytext()); }
"||"		{ return newSymbol(Sym.LOGIC_OR, yytext()); }
"++"		{ return newSymbol(Sym.PLUS_PLUS, yytext()); }
"--"		{ return newSymbol(Sym.MINUS_MINUS, yytext()); }
"?"			{ return newSymbol(Sym.TERN, yytext()); }
":"			{ return newSymbol(Sym.COLON, yytext()); }

"~"			{ return newSymbol(Sym.TILDA, yytext()); }
"^"			{ return newSymbol(Sym.POWER, yytext()); }
"@"			{ return newSymbol(Sym.AT, yytext()); }
"&"			{ return newSymbol(Sym.AMP, yytext()); }
"$"			{ return newSymbol(Sym.DOLAR, yytext()); }

\"          { 
				stringBuffer.setLength(0); 
				yybegin(STRING); 
			}

\'			{ 
				stringBuffer.setLength(0); 
				yybegin(CHAR); 
			}

"//" 		     { yybegin(COMMENT); }
<COMMENT> .      { yybegin(COMMENT); }
<COMMENT> "\r\n" { yybegin(YYINITIAL); }


("true"|"false") 			{ return newSymbol(Sym.BOOL, yytext()); } 

[0-9_][a-zA-Z][a-zA-Z0-9_]+ { lexicError();} 

[0-9]+ 						{ return newSymbol(Sym.NUMBER, new Integer(yytext())); }
[a-zA-Z][a-zA-Z0-9_]* 		{return newSymbol(Sym.IDENT, yytext()); }

<STRING> [^\n\t\r\"\'\\]* { stringBuffer.append( yytext() ); }
<STRING> \\t          	{ stringBuffer.append('\t'); }   
<STRING> \\n          	{ stringBuffer.append('\n'); }
<STRING> \\r          	{ stringBuffer.append('\r'); }
<STRING> \\\\           { stringBuffer.append('\\'); }
<STRING> \\\"         	{ stringBuffer.append('\"'); }
<STRING> \\\'         	{ stringBuffer.append('\''); }
<STRING> \" 		  	{ 
							yybegin(YYINITIAL); 
							return newSymbol(Sym.STRING, stringBuffer.toString()); 
						}
<STRING> \\        		{ lexicError();}
<STRING> "\r\n"    		{ 
							yybegin(YYINITIAL); 
							lexicError();
						}

<CHAR> [^\n\r\t\"\'\\]  { stringBuffer.append(yytext()); } 

<CHAR> \\t				{ stringBuffer.append('\t'); }
<CHAR> \\n				{ stringBuffer.append('\n'); }
<CHAR> \\r 				{ stringBuffer.append('\r'); }
<CHAR> \\0 				{ stringBuffer.append('\0'); }
<CHAR> \" 				{ stringBuffer.append('"'); }
<CHAR> \\\"         	{ stringBuffer.append('"'); }
<CHAR> \\\'         	{ stringBuffer.append('\''); }
<CHAR> \\\\				{ stringBuffer.append('\\'); }
<CHAR> \'         		{ 
							yybegin(YYINITIAL); 
							
							if (stringBuffer.length() == 1)
								return newSymbol(Sym.CHAR, stringBuffer.toString().charAt(0)); 
							
							lexicError(); 
							 
						}
<CHAR> "\r\n"		    { 
							yybegin(YYINITIAL); 
							
							lexicError();
						}

.							{ lexicError(); }
