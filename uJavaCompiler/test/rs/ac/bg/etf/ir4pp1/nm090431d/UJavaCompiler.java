package rs.ac.bg.etf.ir4pp1.nm090431d;

import java.io.*;
import java.time.ZonedDateTime;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import rs.ac.bg.etf.ir4pp1.nm090431d.util.Log4JUtils;
import rs.etf.pp1.mj.runtime.Code;

public class UJavaCompiler
{

    static
    {
	DOMConfigurator.configure(Log4JUtils.instance().findLoggerConfigFile());
    }

    public static void main(String[] args) throws Exception
    {

	Logger log = Logger.getLogger(UJavaCompiler.class);

	log.info(ZonedDateTime.now().toLocalTime());

	Reader br = null;
	try
	{
	    String filepath = "test/program.mj";
	    String output = "output/program.obj";
	    
	    if(args.length == 2)
	    {
		filepath = args[0];
		output = args[1];
	    }
	    
	    File sourceCode = new File(filepath);
	    log.info("Compiling source file: " + sourceCode.getAbsolutePath());

	    br = new BufferedReader(new FileReader(sourceCode));
	    Yylex lexer = new Yylex(br);

	    UJParser p = new UJParser(lexer);
	    p.parse(); // pocetak parsiranja

	    if(!p.errorDetected)
	    {		
		Code.write(new FileOutputStream(output));
		
		log.info("Program uspesno kompajliran: " + output);
	    }
	    else
	    {
		log.error("Kompajliranje nije bilo uspesno.");
	    }

	}
	finally
	{
	    if (br != null) try
	    {
		br.close();
	    }
	    catch (IOException e1)
	    {
		log.error(e1.getMessage(), e1);
	    }
	}

    }

}
